"""
WalBulb
Set LB130 bulb to Pywal color
By Kat Hamer
"""

from colorutils import Color
import pyHS100
import sys
import json

if len(sys.argv) > 1:
    bulb = pyHS100.SmartBulb(sys.argv[1])
else:
    print("Usage: walbulb.py <host>")
    exit()

def main():
    with open("/home/kat/.cache/wal/colors.json") as fp:
        colors = json.load(fp)["colors"]
    color = Color(hex=colors["color1"])
    hue, saturation, value = color.hsv
    if bulb.is_color:
        bulb.hsv = (int(hue), int(saturation*100), int(value*100))
    
if __name__ == "__main__":
    main()
